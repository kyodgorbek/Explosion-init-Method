# Explosion-init-Method
Explosion init method
Q.Explosion = Q.GameObject.extend({
    init: function(x,y,vx,vy,imageData) {
	    
	// Setup a container for our pixels
	this.particles = []
	    
	// Grab the lander's image data
	var landerData = imgData.data;
	    
       // Create a 3x3 pixel data
      // image data container to use for blitting down the road
      this.pixelData = Q.ctx.createImageDate(3,3);
      this.drawPixel =this.pixelData.data;
	
     // Pixels are going to be exploiding out from
    // the center of the lander
   var centerX = imageData.width / 2;
   var centerY = imageData.height / 2;
   
   //Loop over each fourth pixel of the lander image
  for(var sy=0; sy < imgData.height; sy+=4) {
     for(var sx=0;sx< imgData.width;sx+=4)   {
	     
	// Offset into the 1 dimension pixel data array
	var loc = sx*4 + sy * imageData.width * 4;
	     
	// if there's a lander pixel here
	if(landerData[loc + 3])  {
		
	   // Get the direction of the pixel from center
	   var distX sx - centerX;
	   var distY sy - centerY;
		
   	//Add a new particle
	  this.particular.push({
  	 x: x + sx, // starting position x
  	 y: y + sy, // starting positon y
	   lifetime: 5, // remaining lifetime
	   r: landerData[loc] + 20, // make it a little redder
	   g: landerData[loc+1],
	   b: landerData[loc+2],
  	 a: landerDatag[loc+3],
	   // for particle velocity, use the ship's
    // velocity, plus a random direction emanating
   // from the center of the ship
  	 vx: vx/6 + distX * 5 *(Math.random()+0,5),
 	  vy: vy/6 + distY * 5 *(Math.random()+0,5)
	});
  Q.Explosion = Q.GameObject.extend({
    init: function(x,y,vx,vy,imageData) {
	    
	// Setup a container for our pixels
	this.particles = []
	    
	// Grab the lander's image data
	var landerData = imgData.data;
	    
       // Create a 3x3 pixel data
      // image data container to use for blitting down the road
      this.pixelData = Q.ctx.createImageDate(3,3);
      this.drawPixel =this.pixelData.data;
	
     // Pixels are going to be exploiding out from
    // the center of the lander
   var centerX = imageData.width / 2;
   var centerY = imageData.height / 2;
   
   //Loop over each fourth pixel of the lander image
  for(var sy=0; sy < imgData.height; sy+=4) {
     for(var sx=0;sx< imgData.width;sx+=4)   {
	     
	// Offset into the 1 dimension pixel data array
	var loc = sx*4 + sy * imageData.width * 4;
	     
	// if there's a lander pixel here
	if(landerData[loc + 3])  {
		
	   // Get the direction of the pixel from center
	   var distX sx - centerX;
	   var distY sy - centerY;
		
	//Add a new particle
	this.particular.push({
	x: x + sx, // starting position x
	y: y + sy, // starting positon y
	lifetime: 5, // remaining lifetime
	r: landerData[loc] + 20, // make it a little redder
	g: landerData[loc+1],
	b: landerData[loc+2],
	a: landerDatag[loc+3],
	// for particle velocity, use the ship's
	// velocity, plus a random direction emanating
	// from the center of the ship
	vx: vx/6 + distX * 5 *(Math.random()+0,5),
	vy: vy/6 + distY * 5 *(Math.random()+0,5)
	});
   }   }
   Q.Explosion = Q.GameObject.extend({
    init: function(x,y,vx,vy,imageData) {
	    
	// Setup a container for our pixels
	this.particles = []
	    
	// Grab the lander's image data
	var landerData = imgData.data;
	    
       // Create a 3x3 pixel data
      // image data container to use for blitting down the road
      this.pixelData = Q.ctx.createImageDate(3,3);
      this.drawPixel =this.pixelData.data;
	
     // Pixels are going to be exploiding out from
    // the center of the lander
   var centerX = imageData.width / 2;
   var centerY = imageData.height / 2;
   
   //Loop over each fourth pixel of the lander image
  for(var sy=0; sy < imgData.height; sy+=4) {
     for(var sx=0;sx< imgData.width;sx+=4)   {
	     
	// Offset into the 1 dimension pixel data array
	var loc = sx*4 + sy * imageData.width * 4;
	     
	// if there's a lander pixel here
	if(landerData[loc + 3])  {
		
	   // Get the direction of the pixel from center
	   var distX sx - centerX;
	   var distY sy - centerY;
		
	//Add a new particle
	this.particular.push({
	x: x + sx, // starting position x
	y: y + sy, // starting positon y
	lifetime: 5, // remaining lifetime
	r: landerData[loc] + 20, // make it a little redder
	g: landerData[loc+1],
	b: landerData[loc+2],
	a: landerDatag[loc+3],
	// for particle velocity, use the ship's
	// velocity, plus a random direction emanating
	// from the center of the ship
	vx: vx/6 + distX * 5 *(Math.random()+0,5),
	vy: vy/6 + distY * 5 *(Math.random()+0,5)
  	});
   }
  }  
 },
}
